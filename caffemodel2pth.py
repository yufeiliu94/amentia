"""
Caffemodel 到 PyTorch 的转换工具
"""

import os
import sys
import torch
import numpy as np

try:
    from ctypes import cdll
    # TODO 解决硬编码的 protobuf.so 路径
    cdll.LoadLibrary('/usr/lib/x86_64-linux-gnu/libprotobuf.so')
except:
    import warnings
    warnings.warn('Failed loading Protocol Buffer C++ Library')
else:
    os.environ.setdefault("PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION", "cpp")
finally:
    import caffe


def extract_blob(blob) -> np.array:
    return np.array(blob.data).reshape(tuple(blob.shape.dim))

def extract_conv(layer):
    weights = extract_blob(layer.blobs[0])
    if len(layer.blobs) is 2:
        bias = extract_blob(layer.blobs[1])
    else:
        bias = None
    return weights, bias

def load_caffemodel(filename):
    net = caffe.proto.caffe_pb2.NetParameter()
    print('Loading:', filename)
    with open(filename, 'rb') as caffemodel:
        net.MergeFromString(caffemodel.read())
    return net

def extract(model):
    state_dict = {}
    for layer in model.layer:
        if layer.type == 'Convolution' or layer.type == 'Deconvolution':
            print(layer.name)
            weights, bias = extract_conv(layer)
            state_dict['{}.weight'.format(layer.name)] = torch.FloatTensor(weights)
            if bias is not None:
                state_dict['{}.bias'.format(layer.name)] = torch.FloatTensor(bias)
    return state_dict

if __name__ == '__main__':
    model = sys.argv[-2]
    state_dict = extract(load_caffemodel(model))
    torch.save(state_dict, sys.argv[-1])
    from fcn import FCN
    for key, tensor in state_dict.items():
        print(key, tensor.size())
    print('----------------')
    fcn = FCN()
    for key, param in fcn.named_parameters():
        print(key, param.size())
        assert key in state_dict
        assert param.size() == state_dict[key].size()
