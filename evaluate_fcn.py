"""
在 VOC 2012 验证集上评估 FCN 的性能
"""

import torch
import numpy as np
from torch.autograd import Variable
from amentia.metric import ConfusionMatrix
from amentia.dataset import VOC2012SegmentationClass
from fcn import FCN
import datetime

def parse_args():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('voc_dir', type=str)
    parser.add_argument('weights', type=str)
    return parser.parse_args()

def main():
    args = parse_args()
    print('Creating dataset...')
    ds = VOC2012SegmentationClass(args.voc_dir, 'val', mean=np.array((104.00698793, 116.66876762, 122.67891434)))
    ds.serials = [s.strip() for s in open(ds.root_dir + '/ImageSets/Segmentation/seg11valid.txt').readlines()]
    print('Creating model...')
    model = FCN()
    print('Loading weights...')
    model.load_state_dict(torch.load(args.weights))
    print('Uploading model...')
    model.cuda()
    cm = ConfusionMatrix(21)
    start = datetime.datetime.now()
    for i in range(len(ds)):
        print('Processing', i + 1, len(ds), ds.serials[i])
        x, y = ds[i]
        h = model(Variable(x.cuda()))
        pred = torch.max(h, 1)[1]
        cm.update_hist(pred.data.cpu().numpy(), y.numpy())
    cm.report()
    print(datetime.datetime.now() - start)

if __name__ == '__main__':
    main()

