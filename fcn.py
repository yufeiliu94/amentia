import torch


class FCN(torch.nn.Module):
    def __init__(self, num_classes=21):
        import torch.nn as nn
        super().__init__()
        self.conv1_1 = nn.Conv2d(in_channels=3, out_channels=64, kernel_size=3, padding=100)
        self.conv1_2 = nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, padding=1)
        self.conv2_1, self.conv2_2 = self._conv_stage(in_channels=64, out_channels=128, layers=2)
        self.conv3_1, self.conv3_2, self.conv3_3 = self._conv_stage(in_channels=128, out_channels=256, layers=3)
        self.conv4_1, self.conv4_2, self.conv4_3 = self._conv_stage(in_channels=256, out_channels=512, layers=3)
        self.conv5_1, self.conv5_2, self.conv5_3 = self._conv_stage(in_channels=512, out_channels=512, layers=3)
        self.fc6 = nn.Conv2d(in_channels=512, out_channels=4096, kernel_size=7)
        self.fc7 = nn.Conv2d(in_channels=4096, out_channels=4096, kernel_size=1)
        self.score_fr = nn.Conv2d(in_channels=4096, out_channels=num_classes, kernel_size=1)
        self.score_pool4 = nn.Conv2d(in_channels=512, out_channels=num_classes, kernel_size=1)
        self.score_pool3 = nn.Conv2d(in_channels=256, out_channels=num_classes, kernel_size=1)
        self.biliear_kernel_4 = torch.autograd.Variable(self._bilinear_kernel(4, num_classes))
        self.biliear_kernel_16 = torch.autograd.Variable(self._bilinear_kernel(16, num_classes))
        self.stages = [[self.conv1_1, self.conv1_2],
                       [self.conv2_1, self.conv2_2],
                       [self.conv3_1, self.conv3_2, self.conv3_3],
                       [self.conv4_1, self.conv4_2, self.conv4_3],
                       [self.conv5_1, self.conv5_2, self.conv5_3]]

    def forward(self, image):
        import torch.nn.functional as F
        x = image
        pool = [None] * (len(self.stages) + 1)
        for i, stage in enumerate(self.stages, start=1):
            for j, layer in enumerate(stage, start=1):
                x = F.relu(layer(x), inplace=True)
            x = F.max_pool2d(x, kernel_size=2, ceil_mode=True)
            pool[i] = x
        x = F.dropout2d(F.relu(self.fc6(x), inplace=True))
        x = F.dropout2d(F.relu(self.fc7(x)))
        score_pool3 = self.score_pool3(pool[3])
        score_pool4 = self.score_pool4(pool[4])
        score_fr = self.score_fr(x)
        fuse_pool4 = self._fuse(score_fr, score_pool4, ratio=2, offset=5)
        fuse_pool3 = self._fuse(fuse_pool4, score_pool3, ratio=2, offset=9)
        upscore8 = self._upsample(fuse_pool3, ratio=8)
        score = self._cut_down_to(upscore8, target=image, offset=31)
        return score
    
    def cuda(self):
        super().cuda()
        self.biliear_kernel_4 = torch.autograd.Variable(self.biliear_kernel_4.data.cuda())
        self.biliear_kernel_16 = torch.autograd.Variable(self.biliear_kernel_16.data.cuda())

    def cpu(self):
        super().cpu()
        self.biliear_kernel_4 = torch.autograd.Variable(self.biliear_kernel_4.data.cpu())
        self.biliear_kernel_16 = torch.autograd.Variable(self.biliear_kernel_16.data.cpu())

    def _fuse(self, lhs, rhs, ratio=2, offset=0):
        up = self._upsample(lhs, ratio=ratio)
        crop = self._cut_down_to(rhs, target=up, offset=offset)
        return up + crop

    def _upsample(self, x, ratio=2):
        if ratio == 2:
            return torch.nn.functional.conv_transpose2d(x, self.biliear_kernel_4, stride=2)
        if ratio == 8:
            return torch.nn.functional.conv_transpose2d(x, self.biliear_kernel_16, stride=8)

    def _conv_stage(self, in_channels, out_channels, layers):
        xs = [torch.nn.Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=3, padding=1)]
        for i in range(layers - 1):
            xs.append(torch.nn.Conv2d(in_channels=out_channels, out_channels=out_channels, kernel_size=3, padding=1))
        return xs

    def _cut_down_to(self, x, target, offset):
        _, _, target_h, target_w = target.size()
        return x[:, :, offset:offset + target_h, offset:offset + target_w]

    def _bilinear_kernel(self, size: int, channels) -> torch.FloatTensor:
        """
        Make a 2D bilinear kernel suitable for upsampling of the given (h, w) size.
        """
        import numpy as np
        factor = (size + 1) // 2
        if size % 2 == 1:
            center = factor - 1
        else:
            center = factor - 0.5
        og = np.ogrid[:size, :size]
        mat = (1 - abs(og[0] - center) / factor) * (1 - abs(og[1] - center) / factor)
        kernel = np.zeros([channels, channels, size, size], dtype=np.float32)
        for i in range(channels):
            kernel[i, i, :, :] = mat
        return torch.from_numpy(kernel)

