# encoding: UTF-8

import torch.utils.data
import PIL.Image
import skimage.io
import os
import torch
import numpy as np


class VOC2012:
    mean = np.array((104.00698793, 116.66876762, 122.67891434))
    colormap = np.array([
        [  0,   0,   0],
        [128,   0,   0],
        [  0, 128,   0],
        [128, 128,   0],
        [  0,   0, 128],
        [128,   0, 128],
        [  0, 128, 128],
        [128, 128, 128],
        [ 64,   0,   0],
        [192,   0,   0],
        [ 64, 128,   0],
        [192, 128,   0],
        [ 64,   0, 128],
        [192,   0, 128],
        [ 64, 128, 128],
        [192, 128, 128],
        [  0,  64,   0],
        [128,  64,   0],
        [  0, 192,   0],
        [128, 192,   0],
        [  0,  64, 128],
        [128,  64, 128],
        [  0, 192, 128],
        [128, 192, 128],
        [ 64,  64,   0],
        [192,  64,   0],
        [ 64, 192,   0],
        [192, 192,   0],
        [ 64,  64, 128],
        [192,  64, 128],
        [ 64, 192, 128],
        [192, 192, 128],
        [  0,   0,  64],
        [128,   0,  64],
        [  0, 128,  64],
        [128, 128,  64],
        [  0,   0, 192],
        [128,   0, 192],
        [  0, 128, 192],
        [128, 128, 192],
        [ 64,   0,  64],
        [192,   0,  64],
        [ 64, 128,  64],
        [192, 128,  64],
        [ 64,   0, 192],
        [192,   0, 192],
        [ 64, 128, 192],
        [192, 128, 192],
        [  0,  64,  64],
        [128,  64,  64],
        [  0, 192,  64],
        [128, 192,  64],
        [  0,  64, 192],
        [128,  64, 192],
        [  0, 192, 192],
        [128, 192, 192],
        [ 64,  64,  64],
        [192,  64,  64],
        [ 64, 192,  64],
        [192, 192,  64],
        [ 64,  64, 192],
        [192,  64, 192],
        [ 64, 192, 192],
        [192, 192, 192],
        [ 32,   0,   0],
        [160,   0,   0],
        [ 32, 128,   0],
        [160, 128,   0],
        [ 32,   0, 128],
        [160,   0, 128],
        [ 32, 128, 128],
        [160, 128, 128],
        [ 96,   0,   0],
        [224,   0,   0],
        [ 96, 128,   0],
        [224, 128,   0],
        [ 96,   0, 128],
        [224,   0, 128],
        [ 96, 128, 128],
        [224, 128, 128],
        [ 32,  64,   0],
        [160,  64,   0],
        [ 32, 192,   0],
        [160, 192,   0],
        [ 32,  64, 128],
        [160,  64, 128],
        [ 32, 192, 128],
        [160, 192, 128],
        [ 96,  64,   0],
        [224,  64,   0],
        [ 96, 192,   0],
        [224, 192,   0],
        [ 96,  64, 128],
        [224,  64, 128],
        [ 96, 192, 128],
        [224, 192, 128],
        [ 32,   0,  64],
        [160,   0,  64],
        [ 32, 128,  64],
        [160, 128,  64],
        [ 32,   0, 192],
        [160,   0, 192],
        [ 32, 128, 192],
        [160, 128, 192],
        [ 96,   0,  64],
        [224,   0,  64],
        [ 96, 128,  64],
        [224, 128,  64],
        [ 96,   0, 192],
        [224,   0, 192],
        [ 96, 128, 192],
        [224, 128, 192],
        [ 32,  64,  64],
        [160,  64,  64],
        [ 32, 192,  64],
        [160, 192,  64],
        [ 32,  64, 192],
        [160,  64, 192],
        [ 32, 192, 192],
        [160, 192, 192],
        [ 96,  64,  64],
        [224,  64,  64],
        [ 96, 192,  64],
        [224, 192,  64],
        [ 96,  64, 192],
        [224,  64, 192],
        [ 96, 192, 192],
        [224, 192, 192],
        [  0,  32,   0],
        [128,  32,   0],
        [  0, 160,   0],
        [128, 160,   0],
        [  0,  32, 128],
        [128,  32, 128],
        [  0, 160, 128],
        [128, 160, 128],
        [ 64,  32,   0],
        [192,  32,   0],
        [ 64, 160,   0],
        [192, 160,   0],
        [ 64,  32, 128],
        [192,  32, 128],
        [ 64, 160, 128],
        [192, 160, 128],
        [  0,  96,   0],
        [128,  96,   0],
        [  0, 224,   0],
        [128, 224,   0],
        [  0,  96, 128],
        [128,  96, 128],
        [  0, 224, 128],
        [128, 224, 128],
        [ 64,  96,   0],
        [192,  96,   0],
        [ 64, 224,   0],
        [192, 224,   0],
        [ 64,  96, 128],
        [192,  96, 128],
        [ 64, 224, 128],
        [192, 224, 128],
        [  0,  32,  64],
        [128,  32,  64],
        [  0, 160,  64],
        [128, 160,  64],
        [  0,  32, 192],
        [128,  32, 192],
        [  0, 160, 192],
        [128, 160, 192],
        [ 64,  32,  64],
        [192,  32,  64],
        [ 64, 160,  64],
        [192, 160,  64],
        [ 64,  32, 192],
        [192,  32, 192],
        [ 64, 160, 192],
        [192, 160, 192],
        [  0,  96,  64],
        [128,  96,  64],
        [  0, 224,  64],
        [128, 224,  64],
        [  0,  96, 192],
        [128,  96, 192],
        [  0, 224, 192],
        [128, 224, 192],
        [ 64,  96,  64],
        [192,  96,  64],
        [ 64, 224,  64],
        [192, 224,  64],
        [ 64,  96, 192],
        [192,  96, 192],
        [ 64, 224, 192],
        [192, 224, 192],
        [ 32,  32,   0],
        [160,  32,   0],
        [ 32, 160,   0],
        [160, 160,   0],
        [ 32,  32, 128],
        [160,  32, 128],
        [ 32, 160, 128],
        [160, 160, 128],
        [ 96,  32,   0],
        [224,  32,   0],
        [ 96, 160,   0],
        [224, 160,   0],
        [ 96,  32, 128],
        [224,  32, 128],
        [ 96, 160, 128],
        [224, 160, 128],
        [ 32,  96,   0],
        [160,  96,   0],
        [ 32, 224,   0],
        [160, 224,   0],
        [ 32,  96, 128],
        [160,  96, 128],
        [ 32, 224, 128],
        [160, 224, 128],
        [ 96,  96,   0],
        [224,  96,   0],
        [ 96, 224,   0],
        [224, 224,   0],
        [ 96,  96, 128],
        [224,  96, 128],
        [ 96, 224, 128],
        [224, 224, 128],
        [ 32,  32,  64],
        [160,  32,  64],
        [ 32, 160,  64],
        [160, 160,  64],
        [ 32,  32, 192],
        [160,  32, 192],
        [ 32, 160, 192],
        [160, 160, 192],
        [ 96,  32,  64],
        [224,  32,  64],
        [ 96, 160,  64],
        [224, 160,  64],
        [ 96,  32, 192],
        [224,  32, 192],
        [ 96, 160, 192],
        [224, 160, 192],
        [ 32,  96,  64],
        [160,  96,  64],
        [ 32, 224,  64],
        [160, 224,  64],
        [ 32,  96, 192],
        [160,  96, 192],
        [ 32, 224, 192],
        [160, 224, 192],
        [ 96,  96,  64],
        [224,  96,  64],
        [ 96, 224,  64],
        [224, 224,  64],
        [ 96,  96, 192],
        [224,  96, 192],
        [ 96, 224, 192],
        [224, 224, 192]
    ])


class VOC2012SegmentationClass(torch.utils.data.Dataset):
    def __init__(self, root_dir, subset, mean=None):
        assert os.path.isdir(root_dir)
        assert subset in {'train', 'val', 'trainval'}
        self.root_dir = root_dir
        self.mean = mean
        with open(os.path.join(self.root_dir, 'ImageSets/Segmentation/{}.txt'.format(subset))) as serials:
            self.serials = [serial.strip() for serial in serials.readlines()]

    def expand(self, x):
        while len(x.shape) < 4:
            x = np.expand_dims(x, axis=0)
        return x

    def load(self, serial):
        image = os.path.join(self.root_dir, 'JPEGImages', '{}.jpg'.format(serial))
        truth = os.path.join(self.root_dir, 'SegmentationClass', '{}.png'.format(serial))
        x = skimage.io.imread(image)[:, :, ::-1].astype(np.float32)
        if self.mean is not None:
            x -= self.mean
        x = x.transpose([2, 0, 1]).astype(np.float32)
        y = np.array(PIL.Image.open(truth), dtype=np.int64)
        return torch.from_numpy(self.expand(x)), torch.from_numpy(self.expand(y))

    def __len__(self):
        return len(self.serials)

    def __getitem__(self, index):
        return self.load(self.serials[index])

