# encoding: UTF-8

import torch
import torch.autograd
import numpy

def to_numpy(whatever):
    if isinstance(whatever, torch.autograd.Variable):
        whatever = whatever.data
    if torch.is_tensor(whatever):
        whatever = whatever.cpu().numpy()
    if not isinstance(whatever, numpy.ndarray):
        whatever = numpy.array(whatever)
    return whatever

