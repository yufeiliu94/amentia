# encoding: UTF-8

import functools
import amentia.utils
import torch


def wrap_matplotlib(fn):

    def __matplotlib_function__(plt, expr, *args, **kwargs):
        val = fn(expr, *args, **kwargs)
        return plt.imshow(val, *args, **kwargs)

    return __matplotlib_function__


@wrap_matplotlib
def imshow(expr, *args, **kwargs):
    return amentia.utils.to_numpy(expr)


class VOC2012Visualizer:
    def __init__(self):
        self.__colormap__ = torch.from_numpy(amentia.dataset.VOC2012.colormap)
        if torch.cuda.is_available():
            self.__colormap_cuda__ = None

    def colormap_for(self, prediction):
        if prediction.is_cuda:
            if self.__colormap_cuda__ is None:
                self.__colormap_cuda__ = self.__colormap__.cuda()
            return self.__colormap_cuda__
        else:
            return self.__colormap__

    def visualize_label(self, prediction):
        if isinstance(prediction, torch.autograd.Variable):
            prediction = prediction.data
        n, h, w = prediction.size()
        labels = prediction.view(-1)
        colormap = self.colormap_for(prediction)
        return torch.index_select(colormap, 0, labels).view(n, h, w, 3)

    def recover_iamge(self, bgr_minus_mean):
        from amentia.dataset import VOC2012
        b = bgr_minus_mean[0, 0, :, :] + VOC2012.mean[0]
        g = bgr_minus_mean[0, 1, :, :] + VOC2012.mean[1]
        r = bgr_minus_mean[0, 2, :, :] + VOC2012.mean[2]
        return torch.stack((r, g, b), dim=-1)

